#include <iostream>
#include "StateMachine.h"
#include "IdleState.h"
#include <string>

void HandleTransition(const std::string& destination)
{
	std::cout << "Got transition to: " << destination << std::endl;
}

void main()
{
	std::cout << "Hello!" << std::endl;
	StateMachine machine;
	IdleState idle;
	idle.TransitionEvent.Bind(&HandleTransition);
	idle.Enter();

	system("pause");
}