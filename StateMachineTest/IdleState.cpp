#include "IdleState.h"
#include <typeinfo>

IdleState::IdleState()
{
}


IdleState::~IdleState()
{
}

void IdleState::Enter()
{
	Transition(typeid(IdleState).name());
}

void IdleState::Exit()
{
}
