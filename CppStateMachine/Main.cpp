#include <iostream>
#include "Delegate.h"
#include "Signal.h"

void MyFunction(int x)
{
	std::cout << "MyFunction(" << x << ")" << std::endl;
}

class Button
{
public:
	Gallant::Signal2<int, float> updateLabel;

	void Click() const
	{
		updateLabel(2, 99);
	}
};

class Label
{
public:
	void Update(int i, float f) const
	{
		std::cout << "Update(" << i << ", " << f << ")" << std::endl;
	}
};

void main()
{
	Gallant::Delegate1<int> delegate;
	delegate.Bind(&MyFunction);
	delegate(25);

	Button button;
	Label label1;
	button.updateLabel.Connect(&label1, &Label::Update);
	button.Click();

	system("pause");
}