#include "State.h"
#include <algorithm>

State::State()
	:_parent(nullptr)
{
}

void State::AddChildState(std::unique_ptr<State>& state)
{
	auto it = std::find(_childStates.begin(), _childStates.end(), state);
	if (it == _childStates.end())
	{
		throw new std::exception("Cannot add duplicates of states");
	}
	
	_childStates.push_back(std::move(state));
}

void State::Transition(const std::string& destination) const
{
	TransitionEvent(destination);
}
