#include "StateMachine.h"
#include <iostream>
#include <algorithm>

StateMachine::StateMachine()
	:_currentState(nullptr)
{
}

StateMachine::~StateMachine()
{
	for (auto state : _states)
	{
		if (state)
			delete state;
	}
	_states.clear();

	while (_events.size() > 0)
	{
		auto evnt = _events.front();
		_events.pop();
		if (evnt)
			delete evnt;
	}

	while (_waitingTransitions.size() > 0)
	{
		auto trans = _waitingTransitions.front();
		_waitingTransitions.pop();
		if (trans)
			delete trans;
	}
	
	if (_currentState)
	{
		delete _currentState;
		_currentState = nullptr;
	}
}

void StateMachine::Update()
{
	PerformTransitions();
	PumpEvents();
}

void StateMachine::PushEvent(IStateEvent* IStateEvent)
{
	std::lock_guard<std::mutex> lock(_eventmutex);
	_events.push(IStateEvent);
}

void StateMachine::SetInitialState(const std::string& stateTypeName)
{
	auto foundType = std::find_if(_states.begin(), _states.end(),
		[&](State* state)
	{
		return stateTypeName.compare(typeid(state).name()) == 0;
	});
}

void StateMachine::AddState(State* state)
{
	if (std::find(_states.begin(), _states.end(), state)!= _states.end())
		throw new std::exception("You cannot add duplicate states");

	_states.push_back(std::move(state));
}

void StateMachine::PerformTransitions()
{
	if (_waitingTransitions.size() == 0)
		return;

	auto transition = std::move(_waitingTransitions.front());
	_waitingTransitions.pop();
	NotifyAncesstorsOfEnterIfExists(std::move(transition));
}

void StateMachine::NotifyAncesstorsOfExitIfExists(State* destination) const
{
	State* lowestAncestor = nullptr;

	auto ancestor = _currentState->GetParent();
	while (ancestor != nullptr)
	{
		auto destinationAncestor = destination->GetParent();
		while (destinationAncestor != nullptr)
		{
			if (destinationAncestor == ancestor)
			{
				lowestAncestor = destinationAncestor;
			}
			destinationAncestor = destinationAncestor->GetParent();
		}
		ancestor = ancestor->GetParent();
	}

	if (lowestAncestor != nullptr)
	{
		std::cout << "Found lowest common ancestory: " << typeid(destination).name() << std::endl;

		std::vector<State*> graph;
		graph.push_back(_currentState);
		auto nextAncestor = _currentState->GetParent();

		while (nextAncestor != nullptr)
		{
			graph.push_back(nextAncestor);
			nextAncestor = nextAncestor->GetParent();
		}

		graph.erase(graph.begin() + graph.size() - 1);

		std::for_each(graph.begin(), graph.end(), [](State* state)
		{
			std::cout << "Exiting " << typeid(state).name();
			state->Exit();
		});
	}
}

void StateMachine::NotifyAncesstorsOfEnterIfExists(State* destination) const
{
	State* lowestAncestor = nullptr;

	auto ancestor = _currentState->GetParent();
	while (ancestor != nullptr)
	{
		auto destinationAncestor = destination->GetParent();
		while (destinationAncestor != nullptr)
		{
			if (destinationAncestor == ancestor)
			{
				lowestAncestor = destinationAncestor;
			}
			destinationAncestor = destinationAncestor->GetParent();
		}
		ancestor = ancestor->GetParent();
	}

	if (lowestAncestor != nullptr)
	{
		std::cout << "Found lowest common ancestory: " << typeid(destination).name() << std::endl;

		std::vector<State*> graph;
		graph.push_back(_currentState);
		auto nextAncestor = _currentState->GetParent();

		while (nextAncestor != nullptr)
		{
			graph.push_back(nextAncestor);
			nextAncestor = nextAncestor->GetParent();
		}

		graph.erase(graph.begin() + graph.size() - 1);
		std::reverse(graph.begin(), graph.end());

		std::for_each(graph.begin(), graph.end(), [](State* state)
		{
			std::cout << "Exiting " << typeid(state).name();
			state->Exit();
		});
	}
}

void StateMachine::HandleTransition(const std::string& stateTypeName)
{
	State* found = nullptr;

	for (auto state : _states)
	{
		if (stateTypeName.compare(typeid(*state).name()) == 0)
		{
			found = state;
		}
	}

	if (!found)
	{
		throw new std::exception("Cannot transition to a state that doesnt exist.");
	}

	std::cout << "Exiting " << stateTypeName;

	NotifyAncesstorsOfExitIfExists(found);
	_waitingTransitions.push(found);
}

void StateMachine::PumpEvents()
{
	if (_events.empty())
		return;

	auto evnt = _events.front();
	_events.pop();

	auto it = _currentState->EventsSubscribed.find(typeid(evnt).name());
	if (it == _currentState->EventsSubscribed.end())
	{
		std::string msg = "Could not find event halder for event ";
		msg += typeid(evnt).name();
		throw new std::exception(msg.c_str());
	}

	auto handler = _currentState->EventsSubscribed[typeid(evnt).name()];
	handler(*evnt);

	auto parent = _currentState->GetParent();
	while (parent != nullptr)
	{
		auto find = parent->EventsSubscribed.find(typeid(evnt).name());
		if (find != parent->EventsSubscribed.end())
		{
			auto handler = find->second;
			handler(*evnt);
		}
		parent = parent->GetParent();
	}
}

