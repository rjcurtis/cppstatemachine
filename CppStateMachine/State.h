#pragma once

#include <vector>
#include <memory>
#include <typeinfo>
#include <map>
#include <functional>
#include "IStateEvent.h"
#include "Delegate.h"

class State
{
	typedef std::function<void(IStateEvent)> EventCallback;

public:
	State();
	virtual ~State(){}
	State(const State&) = delete;
	State& operator=(const State&) = delete;

	Gallant::Delegate1<const std::string&> TransitionEvent;
	std::map <const std::string, EventCallback> EventsSubscribed;

	virtual void Enter() = 0;
	virtual void Exit() = 0;

	void AddChildState(std::unique_ptr<State>& state);
	inline State* GetParent() const { return _parent; }

protected:
	void Transition(const std::string& destination) const;
	
private:
	State* _parent;
	std::vector<std::unique_ptr<State>> _childStates;
};

