#pragma once

#include <vector>
#include <queue>
#include <memory>
#include <mutex>
#include <string>
#include "../StateMachineTest/IdleState.h"

class IStateEvent;

class StateMachine
{
public:
	StateMachine();
	~StateMachine();

	void Update();
	void PushEvent(IStateEvent* IStateEvent);
	void SetInitialState(const std::string& stateTypeName);
	void AddState(State* state);
private:
	std::mutex _eventmutex;
	std::mutex _transitionMutex;
	std::vector<State*> _states;
	std::queue<IStateEvent*> _events;
	std::queue<State*> _waitingTransitions;
	State* _currentState;

	void PerformTransitions();
	void NotifyAncesstorsOfEnterIfExists(State* destination) const;
	void NotifyAncesstorsOfExitIfExists(State* destination) const;
	void HandleTransition(const std::string& stateTypeName);
	void PumpEvents();
};

